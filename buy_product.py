import psycopg2

price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"
inventory_size = "SELECT SUM(amount) FROM Inventory WHERE username = %(username)s"
add_to_inventory = "INSERT INTO Inventory(username, product, amount) VALUES (%(username)s, %(product)s, %(amount)s) ON CONFLICT (username, product) DO UPDATE SET amount = Inventory.amount + excluded.amount"


def get_connection():
    return psycopg2.connect(
        dbname="lab7or9",
        user="postgres",
        password="postgres",
        host="localhost",
        port=5432
    )


def buy_product(username, product, amount):
    obj = {"product": product, "username": username, "amount": amount}
    with get_connection() as conn:
        with conn.cursor() as cur:
            try:
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")

                cur.execute(buy_decrease_stock, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")

                cur.execute(inventory_size, obj)
                result = cur.fetchone()
                current_amount = 0 if (result is None or result[0] is None) else result[0]
                if current_amount + amount > 100:
                    raise Exception("Max size for users inventory is 100")

                cur.execute(add_to_inventory, obj)
                conn.commit()
            except Exception as e:
                conn.rollback()
                raise e


buy_product('Alice', 'marshmello', 1)
