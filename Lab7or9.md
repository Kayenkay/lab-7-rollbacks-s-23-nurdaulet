## Lab Implementation

### Task 1
With the following SQL-commands, I obtained the following migrations file:

```sql
CREATE DATABASE lab;
CREATE TABLE Player (username TEXT UNIQUE, balance INT CHECK(balance >= 0));
CREATE TABLE Shop (product TEXT UNIQUE, in_stock INT CHECK(in_stock >= 0), price INT CHECK (price >= 0));
INSERT INTO Player (username, balance) VALUES ('Alice', 100);
INSERT INTO Player (username, balance) VALUES ('Bob', 200);
INSERT INTO Shop (product, in_stock, price) VALUES ('marshmello', 10, 10);
```

### Task 2

Here I've created new "Inventory" table and checking whether the amount of items is applicable for the user
```sql
CREATE TABLE Inventory (
    username TEXT REFERENCES Player(username) NOT NULL,
    product  TEXT REFERENCES Shop(product) NOT NULL,
    amount   INT CHECK (amount >= 0),
    UNIQUE(username, product)
);

CREATE OR REPLACE FUNCTION check_inventory_size() RETURNS TRIGGER AS $$
DECLARE
    total_sum_amount INT;
BEGIN
    SELECT SUM(amount) INTO total_sum_amount FROM Inventory WHERE username = NEW.username;
    IF total_sum_amount > 100 THEN
        RAISE EXCEPTION 'Max size for users inventory is 100';
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;
END;

CREATE TRIGGER inventory_limit BEFORE INSERT ON inventory FOR EACH ROW EXECUTE FUNCTION check_inventory_limit_size();
```